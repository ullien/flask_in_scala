**Flask in Scala**

This is a project repository for the series of articles exploring ScalaCheck tool for testing in Scala language.
I was inspired by an assignment during LINKIT Data Engineering bootcamp to build a simple web application, write tests 
for it and explore CI/CD part of code life-cycle.

**Current stage**

Published articles on ITNEXT:

A newbie way to play with ScalaCheck: part 1 [Generators](https://itnext.io/a-newbie-way-to-play-with-scalacheck-333cbabfc00a)

A newbie way to play with ScalaCheck: part 2 [Properties](https://itnext.io/a-newbie-way-to-play-with-scalacheck-ac409a8205a2)

If you read part 2 and you are curious about solution for division, go to src/test/scala/caskApi/SimpleCalculatorProps. A real doozy, isn't it? 