name := "Flask_in_Scala"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.3" % Test

libraryDependencies ++= Seq(
    "com.lihaoyi" %% "cask" % "0.7.4",
    "com.lihaoyi" %% "requests" % "0.6.0",
    "com.typesafe.play" %% "twirl-api" % "1.5.0"
)

lazy val root = (project in file(".")).enablePlugins(SbtTwirl)

