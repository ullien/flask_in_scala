package caskApi

import cask.Request

object ApplicationEntryPoint extends cask.MainRoutes{
  @cask.get("/")
  def hello() = {
    "Hello World!!"
  }

  @cask.post("/do-thing")
  def doThing(request: cask.Request) = {
    request.text().reverse
  }

  @cask.route("/calculate", methods = Seq("get", "post"))
  def calculator(request: cask.Request) = {
    "<!doctype html>" + html.inputForm("Hello player!")
  }

  @cask.postForm("/calculator")
  def jsonEndpoint(answer: cask.FormValue) = {
    val true_value = answer.value.toString.split(" ")
    val value1 = true_value(0).toInt
    val value2 = true_value(2).toInt
    val value3 = true_value(1)
//    val calc = Calculator()
//    calc.calculator(value1, value2, value3)

  }


  initialize()

}
