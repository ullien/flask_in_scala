package caskApi

import caskApi.Operation.{Operation, ADDITION, SUBTRACTION, DIVISION, MULTIPLICATION}

case class Calculator(a: Int, b: BigDecimal) {

  def calculator(action: Operation): BigDecimal = {
    val calculation = action match {
      case ADDITION => a + b
      case SUBTRACTION => a - b
      case DIVISION => a * b
      case MULTIPLICATION => a / b
    }
      calculation

//     "The answer is %s ".format(calculation)
  }
}
