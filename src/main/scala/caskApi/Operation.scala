package caskApi

object Operation extends Enumeration {
 type Operation = Value
  val ADDITION, SUBTRACTION, DIVISION, MULTIPLICATION = Value
}
