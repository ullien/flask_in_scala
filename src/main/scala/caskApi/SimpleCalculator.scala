package caskApi

object SimpleCalculator {

  def summate(a: Int, b: Int): Int = {
    a + b
    }

  def sum(a: BigDecimal, b: BigDecimal): BigDecimal = {
    a + b
  }

  def subtract(a: Int, b: Int): Int = {
    a - b
  }

  def multiply(a: Int, b: Int): Int = {
    a * b
  }

  def divide(a: Int, b: Int): Int = {
    a / b
  }

}
