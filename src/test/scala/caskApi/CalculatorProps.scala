package caskApi

import java.math.MathContext

import org.scalacheck.Prop.forAll
import caskApi.Operation.{ADDITION, DIVISION, MULTIPLICATION, SUBTRACTION}



import org.scalacheck.{Prop, Properties}
import org.scalacheck.Prop.propBoolean

object CalculatorProps extends Properties ("Calculator") {

  property("ADDITION") = forAll { (a: Int, b: BigDecimal) =>
    val result = Calculator(a, b).calculator(ADDITION).round(new MathContext(4))
    result == (a + b).round(new MathContext(4))  && result == (b + a).round(new MathContext(4))
  }

  property("SUBTRACTION") = forAll { (a: Int, b: BigDecimal) =>
    val result = Calculator(a, b).calculator(SUBTRACTION).round(new MathContext(4))
    result == (a - b).round(new MathContext(4))  && result == (-b + a).round(new MathContext(4))
  }

// TBD: exception raise in MULTIPLICATION (division by zero) and round in DIVISION
  //  property("MULTIPLICATION") = forAll { (a: Int, b: BigDecimal) =>
//    val result = Calculator(a, b).calculator(MULTIPLICATION).round(new MathContext(4))
//    result == (a * b).round(new MathContext(4)) && result == (b * a).round(new MathContext(4))
//  }
//
//  property("DIVISION") = forAll { (a: Int, b: BigDecimal) =>
//    lazy val result = Calculator(a, b).calculator(DIVISION).round(new MathContext(4))
//    if (b == 0)
//      Prop.throws(classOf[ArithmeticException]) {
//        result
//      }
//    else {
//      a == (result * b + (a % b)).round(new MathContext(4))
//    }
//  }

}
