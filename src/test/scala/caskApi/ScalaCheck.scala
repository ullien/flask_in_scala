package caskApi

import caskApi.Operation.Operation
import org.scalacheck.Gen

object ScalaCheck extends App {
  val aGen: Gen[Int] = Gen.choose(-100, 1000)

  val bGen: Gen[BigDecimal] = for {
    value <- Gen.chooseNum(-100000000, 10000000)
    valueDecimal = BigDecimal.valueOf(value)
  } yield valueDecimal

    val operatorGen: Gen[Operation] = Gen.frequency(
      2 -> Operation.MULTIPLICATION, 1 -> Operation.SUBTRACTION,
      4 -> Operation.DIVISION, 5 -> Operation.ADDITION
    )

//    val c = new Calculator()
    val calculatorGen: Gen[Calculator] = for {
      a <- aGen
      b <- bGen
    }  yield Calculator(a, b)

    val methodCalcGen: Gen[BigDecimal] = for {
      c <- calculatorGen
      operator <- operatorGen
    } yield c.calculator(operator)

  println(calculatorGen.sample)
  println(methodCalcGen.sample)

}
