package caskApi

import java.math.MathContext
import org.scalacheck.{Prop, Properties}
import org.scalacheck.Prop.forAll


object SimpleCalculatorProps extends Properties ("SimpleCalculator") {

  property("sum") = forAll { (a: BigDecimal, b: BigDecimal) =>
    val result = SimpleCalculator.sum(a, b).round(new MathContext(4))
    result == (a + b).round(new MathContext(4))  && result == (b + a).round(new MathContext(4))
  }

  property("summate") = forAll { (a: Int, b: Int) =>
    val result = SimpleCalculator.summate(a, b)
    result == a + b && result == b + a
  }

  property("multiply") = forAll { (a: Int, b: Int) =>
    val result = SimpleCalculator.multiply(a, b)
    result == a * b && result == b * a
  }

  property("subtract") = forAll { (a: Int, b: Int) =>
    val result = SimpleCalculator.subtract(a, b)
    result == a - b && result == - b + a
  }

  property("divide") = forAll { (a: Int, b: Int) =>
    lazy val result = SimpleCalculator.divide(a, b)
    if (b == 0)
      Prop.throws(classOf[ArithmeticException]) {
        result
      }
    else {
      a == result * b + (a % b)
    }

  }
}